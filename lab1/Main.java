import java.io.*;
import java.security.*;

public class Main {
    public static void main(String[] args) {
        try {
            String filePath = "C:\\Users\\aleks\\Desktop\\Универ\\6 семестр\\защита данных\\лабы\\doks";

            OtherWay(filePath);

            // Проверка размера и sha1 исходного файла
            //sha1(filePath,"\\leasing.txt");

            String gitDirPath = "C:\\Users\\aleks\\Desktop\\Универ\\6 семестр\\защита данных\\лабы\\doks";
            ProcessBuilder pb = new ProcessBuilder("C:\\Program Files\\Git\\usr\\bin\\openssl.exe", "dgst", "-sha1", gitDirPath + "\\leasing.txt");
            pb.directory(new File(gitDirPath));
            Process p = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            //System.out.println(reader.readLine());
            File file = new File(gitDirPath + "\\leasing.txt");
            long fileSizeInBytes = file.length();
            System.out.println(reader.readLine() + "\n" + "Размер исходного файла " + fileSizeInBytes + " байт");

            if (p.waitFor() != 0) {
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void ProcessBuild(Integer count) {
        try{
            String newFilePath = "C:\\Users\\aleks\\Desktop\\Универ\\6 семестр\\защита данных\\лабы\\doks\\leasing" + count + ".txt";
            ProcessBuilder pb3 = new ProcessBuilder("C:\\Program Files\\Git\\usr\\bin\\openssl.exe", "dgst", "-sha1", newFilePath);
            pb3.directory(new File(newFilePath));
            Process p3 = pb3.start();
            BufferedReader reader3 = new BufferedReader(new InputStreamReader(p3.getInputStream()));
            if (p3.waitFor() != 0) {
            }
            try {
                System.out.println(reader3.readLine());
            }

            //попытка починить sha1(бесполезно)
            finally {
                // Закрытие потоков ввода-вывода
                reader3.close();
                p3.getInputStream().close();
                p3.getOutputStream().close();
                p3.getErrorStream().close();
            }
            int exitCode = p3.waitFor();
            if (exitCode != 0) {
                System.err.println("Process returned non-zero exit code " + exitCode);
            }
            p3.destroy();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void OtherWay(String filePath) {
        try {
            String fileName = "\\leasing.txt";
            String outputFileName;
            BufferedReader reader = new BufferedReader(new FileReader(filePath + fileName));
            String line = reader.readLine();
            int spaceIndex = -1;
            int count = 0;

            while (line != null && count < 300) {
                //&& count < 300)
                spaceIndex = line.indexOf(" ", -1); // Сброс spaceIndex перед каждым поиском
                if (spaceIndex != -1) {
                    // Создание нового файла с измененным пробелом
                    count++;
                    outputFileName = "\\leasing" + count + ".txt";
                    BufferedWriter writer = new BufferedWriter(new FileWriter(filePath + outputFileName));
                    writer.write(line.substring(0, spaceIndex) + "ㅤ" + line.substring(spaceIndex + 1));
                    writer.newLine();

                    // Запись оставшегося текста файла
                    while ((line = reader.readLine()) != null) {
                        writer.write(line);
                        writer.newLine();
                    }
                    writer.close();

                    //определение sha1 и размера
                    sha1(filePath,outputFileName);

                    // Переключение на новый файл и продолжение работы
                    fileName = outputFileName;
                    reader = new BufferedReader(new FileReader(filePath + fileName));
                    line = reader.readLine();
                    spaceIndex = -1;
                } else {
                    // Продолжение обхода файла, если пробелы не найдены в текущей строке
                    line = line + "\n" + reader.readLine();
                }
            }
            reader.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void sha1 (String filePath, String outputFileName){
        try{
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            FileInputStream fis = new FileInputStream(filePath + outputFileName);
            byte[] dataBytes = new byte[1024];
            int bytesRead;
            while ((bytesRead = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, bytesRead);
            }
            byte[] mdBytes = md.digest();

            // Преобразование хеш-кода в строку и вывод на экран
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mdBytes.length; i++) {
                sb.append(Integer.toString((mdBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            String sha1Hash = sb.toString();
            File file = new File(filePath + outputFileName);
            long fileSizeInBytes = file.length();
            System.out.println("SHA-1 файла " + outputFileName + ": " + sha1Hash + ", размер файла " + fileSizeInBytes + " байт");
            //Остановка при совпадении sha1
            if (sha1Hash.equals("16a811e9c6cafab963b1e35f5c5cb6108dee5b64")) {
                System.exit(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}