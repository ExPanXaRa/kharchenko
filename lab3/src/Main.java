import java.io.*;

public class Main {
    public static void main(String[] args) {
        String filePath = "C:\\Users\\aleks\\Desktop\\Универ\\6 семестр\\защита данных\\лабы\\kharchenko\\lab3";
        String KEY = openssl_key();
        byte [] header = decay(filePath);
        openssl_aes(KEY, filePath, "cbc", header);
        openssl_aes(KEY, filePath, "ecb", header);
        openssl_aes(KEY, filePath, "cfb", header);
        openssl_aes(KEY, filePath, "ofb", header);
    }

    //Шифрование изображения
    public static void openssl_aes(String key, String filePath, String mode, byte[] header) {
        try {
            ProcessBuilder pb = new ProcessBuilder("C:\\Program Files\\Git\\usr\\bin\\openssl.exe", "enc", "-aes-256-" + mode, "-in",
                    filePath + "\\files\\tux_tail.md", "-out", filePath + "\\files\\tux_tail_enc" + mode.toUpperCase() + ".md", "-pass", "pass:" + key);
            Process p = pb.start();
            p.waitFor();
            unite(filePath,header,mode);
            if (p.waitFor() != 0) {
                System.out.println("Ошибка в openssl_aes");
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static byte[] decay (String filePath) {
        byte[] header = null;
        try {
            //Чтение файла
            FileInputStream fileInputStream = new FileInputStream(filePath + "\\tux.bmp");
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);
            //Отделение хедера
            header = new byte[122];
            dataInputStream.readFully(header);
            //Сохранение файла с остатком
            FileOutputStream fileOutputStream = new FileOutputStream(filePath + "\\files\\tux_tail.md");
            DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = dataInputStream.read(buffer)) != -1) {
                dataOutputStream.write(buffer, 0, length);
            }
            //Закрытие файлов и возвращение хедера
            dataInputStream.close();
            dataOutputStream.close();
            return header;
        } catch (IOException e) {
            e.printStackTrace();
        } return header;
    }

    public static void unite(String filePath, byte[] header, String mode) {
        try {
            //Чтение зашифрованного тела файла
            FileInputStream restInputStream = new FileInputStream( filePath + "\\files\\tux_tail_enc" + mode.toUpperCase() + ".md");
            ByteArrayOutputStream restOutputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while ((length = restInputStream.read(buffer)) != -1) {
                restOutputStream.write(buffer, 0, length);
            }
            byte[] rest = restOutputStream.toByteArray();
            //Объединение хедера и конца с зашифрованным телом файла
            FileOutputStream fileOutputStream = new FileOutputStream(filePath + "\\tux_" + mode.toUpperCase() + ".bmp");
            fileOutputStream.write(header);
            fileOutputStream.write(rest);
            //Закрытие файлов
            restInputStream.close();
            fileOutputStream.close();
            //Сообщение об успешности операции шифрования
            System.out.println("Файл tux.bmp зашифрован в tux_" + mode.toUpperCase() + ".bmp" + " шифром AES в режиме " + mode.toUpperCase());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Генерация случайного ключа
    public static String openssl_key() {
        String key = null;
        try {
            ProcessBuilder pb = new ProcessBuilder("C:\\Program Files\\Git\\usr\\bin\\openssl.exe", "rand", "-hex", "16");
            Process p = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            key = reader.readLine();
            System.out.println("Случайно сгенерированный ключ: " + key);
            if (p.waitFor() != 0) {
                System.out.println("Ошибка в openssl_key");
            }
            return key;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return key;
    }
}
